//
//  APIConfigurable.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol APIConfigurable {
    var APIKey: String { get }
    var Host: String { get }
    var SearchEndpoint: String { get }
}
