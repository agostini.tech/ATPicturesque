//
//  Photo.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Photo {
    var photoID: String { get }
    var dateCreated: Date { get }
    var dateUpdated: Date { get }
    var description: String? { get }
    var likes: Int { get }
    var thumbURL: URL { get }
    var photoURL: URL { get }
    var author: Author { get }
}
