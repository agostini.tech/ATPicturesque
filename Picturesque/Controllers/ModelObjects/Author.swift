//
//  Author.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Author {
    var authorID: String { get }
    var firstName: String? { get }
    var lastName: String? { get }
    var fullName: String? { get }
}
