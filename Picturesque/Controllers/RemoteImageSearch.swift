//
//  RemoteImageSearch.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol RemoteImageSearch {
    
    /**
     Gets images from the remote source that match the search term.
     
     - parameters:
        - searchTerm: Search term for the image your're searching (e.g. 'Rabbit')
        - page: The next page you want to fetch. If this is your first time calling the function pass in '1'.
        - onCompleted: Completion handler that will return the images for the page and the search term you provided.
                       The handler will return the total number of pages for the specified search term. And the standard error object.
     */
    func getImages(searchTerm: String, page: Int, onCompleted: @escaping (([Photo], Int, Error?)->()))
}
