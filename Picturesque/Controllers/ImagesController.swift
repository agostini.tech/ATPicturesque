//
//  ImagesController.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol ImagesController {
    
    /**
     An array holding the most recent results.
     
     - returns:
     An array with the most recent results.
     */
    var results: [Photo] { get }
    
    /**
     Use this property to check if there are more results to be fetched from the remote server.
     
     - returns:
     true if there are more results that can be loaded.
     */
    var hasMoreResults: Bool { get }
    
    /**
     Clears all the internal variables and resets the state of the class.
     */
    func clearResults()
    
    /**
     Searches for images from the remote images provider (e.g. Unsplash).
     
     - parameters:
        - withSearchTerm: Search term for the image your're searching (e.g. 'Rabbit')
        - onCompleted: Completion handler that will return an array of images if it finds any. The handler returns an error if there was a problem downloading the image.
     */
    func getImages(withSearchTerm searchTerm: String, onCompleted: @escaping (([Photo], Error?) -> ()))
    
    /**
     The data is paginated, so if you want to load the next page of results you will have to call this function.
     
     - parameters:
        - onCompleted: Completion handler that will return an array of images. It will append the new results from the new page at the end of the array.
                       The handler returns an error if there was a problem downloading the image.
     */
    func loadMoreResults(onCompleted: @escaping (([Photo], Error?) -> ()))
    
    /**
     Gets data from the provided url, if the data is cached it will return the cached data.
     
     - parameters:
        - dataURL: URL of the resource.
        - onCompleted: Completion handler that will pass in the original url, fetched data and/or the error.
     */
    func getData(withURL url: URL, onCompleted: @escaping ((URL, Data?, Error?) -> ()))
}
