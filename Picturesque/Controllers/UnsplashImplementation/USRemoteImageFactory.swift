//
//  UnsplashedRemoteImageFactory.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol RemoteImageFactory {
    func getTotalPages(fromDict dict: [String : Any]) -> Int
    func getPhotos(fromDict dict: [String : Any]) -> [Photo]
}

class USRemoteImageFactory: RemoteImageFactory {
    
    private enum JSONKey: String {
        case total_pages
        case results
    }
    
    private enum PhotoKey: String {
        case id
        case created_at
        case updated_at
        case description
        case likes
        case urls
        case full
        case thumb
        case user
    }
    
    private enum AuthorKey: String {
        case id
        case name
        case first_name
        case last_name
    }
    
    private static let dateFormatter = DateFormatter()
    init(_ dateFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ") {
        USRemoteImageFactory.dateFormatter.dateFormat = dateFormat
    }
    
    func getTotalPages(fromDict dict: [String : Any]) -> Int {
        return dict[JSONKey.total_pages.rawValue] as? Int ?? 0
    }
    
    func getPhotos(fromDict dict: [String : Any]) -> [Photo] {
        guard let results = dict[JSONKey.results.rawValue] as? [[String : Any]] else {
            return []
        }
        return results.compactMap { self.getPhoto($0) }
    }
    
    private func getPhoto(_ dict: [String : Any]) -> Photo? {
        guard
            let id = dict[PhotoKey.id.rawValue] as? String,
            let dateCreatedString = dict[PhotoKey.created_at.rawValue] as? String,
            let dateCreated = self.getDate(dateCreatedString),
            let dateUpdatedString = dict[PhotoKey.updated_at.rawValue] as? String,
            let dateUpdated = self.getDate(dateUpdatedString),
            let likes = dict[PhotoKey.likes.rawValue] as? Int,
            let urlsDict = dict[PhotoKey.urls.rawValue] as? [String : Any],
            let fullURLString = urlsDict[PhotoKey.full.rawValue] as? String,
            let fullURL = URL(string: fullURLString),
            let thumbURLString = urlsDict[PhotoKey.thumb.rawValue] as? String,
            let thumbURL = URL(string: thumbURLString),
            let userDict = dict[PhotoKey.user.rawValue] as? [String : Any],
            let author = self.getAuthor(userDict)
            else {
                return nil
        }
        
        return USPhoto(photoID: id,
                       dateCreated: dateCreated,
                       dateUpdated: dateUpdated,
                       description: dict[PhotoKey.description.rawValue] as? String,
                       likes: likes,
                       thumbURL: thumbURL,
                       photoURL: fullURL,
                       author: author)
    }
    
    private func getDate(_ dateString: String) -> Date? {
        return USRemoteImageFactory.dateFormatter.date(from: dateString)
    }
    
    private func getAuthor(_ dict: [String : Any]) -> Author? {
        guard let id = dict[AuthorKey.id.rawValue] as? String else {
            return nil
        }
        return USAuthor(authorID: id,
                        firstName: dict[AuthorKey.first_name.rawValue] as? String,
                        lastName: dict[AuthorKey.last_name.rawValue] as? String,
                        fullName: dict[AuthorKey.name.rawValue] as? String)
    }
}

fileprivate struct USAuthor: Author {
    let authorID: String
    let firstName: String?
    let lastName: String?
    let fullName: String?
}

fileprivate struct USPhoto: Photo {
    let photoID: String
    let dateCreated: Date
    let dateUpdated: Date
    let description: String?
    let likes: Int
    let thumbURL: URL
    let photoURL: URL
    let author: Author
}
