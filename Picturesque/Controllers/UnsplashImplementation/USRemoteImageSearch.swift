//
//  UnsplashedRemoteImageSearch.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

fileprivate struct USConfig: APIConfigurable {
    let APIKey = "_YOUR_API_KEY_"
    let Host = "https://api.unsplash.com"
    let SearchEndpoint = "/search/photos"
}

fileprivate enum USError: Error {
    case CannotCreateURL
    case DidNotReceiveData
    case ErrorConvertingToJSON
    case ErrorCastingToDictionary
}

class USRemoteImageSearch: RemoteImageSearch {
    
    private let config: APIConfigurable
    private let session: URLSession
    private var imagesDataTask: URLSessionDataTask?
    private let factory: RemoteImageFactory
    
    init(withConfig config: APIConfigurable = USConfig(),
         session: URLSession = URLSession(configuration: .default),
         factory: RemoteImageFactory = USRemoteImageFactory())
    {
        self.config = config
        self.session = session
        self.factory = factory
    }
    
    func getImages(searchTerm: String, page: Int, onCompleted: @escaping (([Photo], Int, Error?)->())) {
        
        self.imagesDataTask?.cancel()
        
        let requestString: String = "\(config.Host)\(config.SearchEndpoint)?client_id=\(config.APIKey)&page=\(page)&query=\(searchTerm)"
        if let requestURL = URL(string: requestString) {
            self.imagesDataTask = session.dataTask(with: requestURL, completionHandler: { (data, response, error) in
                guard error == nil else {
                    onCompleted([], 0, error)
                    return
                }
                
                guard let responseData = data else {
                    onCompleted([], 0, USError.DidNotReceiveData)
                    return
                }
                
                do {
                    guard let responseDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String : Any] else {
                        onCompleted([], 0, USError.ErrorCastingToDictionary)
                        return
                    }
                    
                    let pages = self.factory.getTotalPages(fromDict: responseDict)
                    let photos = self.factory.getPhotos(fromDict: responseDict)
                    
                    onCompleted(photos, pages, nil)
                } catch {
                    onCompleted([], 0, USError.ErrorConvertingToJSON)
                    return
                }
            })
            
            self.imagesDataTask?.resume()
        } else {
            onCompleted([], 0, USError.CannotCreateURL)
            return
        }
    }
}


