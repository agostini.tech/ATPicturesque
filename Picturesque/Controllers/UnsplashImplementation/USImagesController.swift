//
//  USImagesController.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

class USImagesController: ImagesController {
    
    private var searchTerm: String = ""
    private var page: Int = 1
    private var totalPages: Int?
    private let remoteSearch: RemoteImageSearch
    private let cache: CachedData
    
    init(_ remoteImageSearch: RemoteImageSearch = USRemoteImageSearch(), cache: CachedData = NSCachedData()) {
        self.remoteSearch = remoteImageSearch
        self.cache = cache
    }
    
    var results: [Photo] = []
    
    var hasMoreResults: Bool {
        guard let total = totalPages else {
            return false
        }
        return page < total
    }
    
    func clearResults() {
        self.results.removeAll()
        self.cache.clearCache()
        self.searchTerm = ""
        self.page = 1
    }
    
    func getImages(withSearchTerm searchTerm: String, onCompleted: @escaping (([Photo], Error?) -> ())) {
        self.clearResults()
        self.searchTerm = searchTerm
        self.getImages(searchTerm, page: self.page, onCompleted: onCompleted)
    }
    
    func loadMoreResults(onCompleted: @escaping (([Photo], Error?) -> ())) {
        self.getImages(self.searchTerm, page: (self.page + 1), onCompleted: onCompleted)
    }
    
    private func getImages(_ searchTerm: String, page: Int, onCompleted: @escaping (([Photo], Error?) -> ())) {
        self.remoteSearch.getImages(searchTerm: searchTerm, page: page) { (items, totalPages, error) in
            guard error == nil else {
                DispatchQueue.main.async {
                    onCompleted([], error)
                }
                return
            }
            
            self.page = page
            self.totalPages = totalPages
            self.results.append(contentsOf: items)
            
            DispatchQueue.main.async {
                onCompleted(self.results, nil)
            }
        }
    }
    
    func getData(withURL url: URL, onCompleted: @escaping ((URL, Data?, Error?) -> ())) {
        self.cache.loadData(url) { (data, error) in
            DispatchQueue.main.async {
                onCompleted(url, data, error)
            }
        }
    }
}
