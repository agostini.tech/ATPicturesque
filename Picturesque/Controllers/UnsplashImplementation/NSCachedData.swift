//
//  NSCachedData.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

private enum NSDataError: Error {
    case FailedToLoadData
}

class NSCachedData: CachedData {
    
    var dataCache = NSCache<NSString, NSData>()
    
    private let session: URLSession
    
    init(session: URLSession = URLSession(configuration: .default)) {
        self.session = session
    }
    
    func loadData(_ dataURL: URL, onCompleted: @escaping ((Data?, Error?) -> ())) {
        if let cachedData = self.dataCache.object(forKey: NSString(string: dataURL.absoluteString)) as Data? {
            onCompleted(cachedData, nil)
            return
        } else {
            self.session.dataTask(with: dataURL) {
                (data, response, error) in
                
                guard
                    let responseData = data
                    else {
                        onCompleted(nil, NSDataError.FailedToLoadData)
                        return
                }
                
                self.dataCache.setObject(responseData as NSData, forKey: NSString(string: dataURL.absoluteString))
                
                onCompleted(responseData, nil)
                
                }.resume()
        }
    }
    
    func clearCache() {
        self.dataCache.removeAllObjects()
    }
}
