//
//  CachedData.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol CachedData {
    
    /**
     Loads data from the specified URL. This function will cache the response
     internally, if you call it for the second time, you will receive cached data.
     
     - parameters:
        - dataURL: URL of the resource to fetch and cache.
        - onCompleted: Completion handler that will pass in the fetched data and/or the error.
     */
    func loadData(_ dataURL: URL, onCompleted: @escaping ((Data?, Error?) -> ()))
    
    /**
     Clears the internal cache :)
     */
    func clearCache()
}
