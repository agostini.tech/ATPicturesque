//
//  ViewController.swift
//  Picturesque
//
//  Created by Dejan on 08/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit
import os.signpost

fileprivate enum Constants: String {
    case ImageCell
}

class ViewController: UITableViewController {
    
    public var controller: ImagesController = USImagesController()
    
    fileprivate let networkingLog = OSLog(subsystem: "tech.agostini.picturesque", category: "NetworkingOperations")
    fileprivate let poiLog = OSLog(subsystem: "tech.agostini.picturesque", category: .pointsOfInterest)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let spidForFirstLoad = OSSignpostID(log: networkingLog)
        os_signpost(.begin, log: networkingLog, name: "Initial Load", signpostID: spidForFirstLoad, "Rabbit - Start")
        controller.getImages(withSearchTerm: "Rabbit") { (items, error) in
            self.tableView.reloadData()
            os_signpost(.end, log: self.networkingLog, name: "Initial Load", signpostID: spidForFirstLoad, "Rabbit - End")
        }
    }
}

// MARK: - UISearchBarDelegate
extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let spidForSearch = OSSignpostID(log: networkingLog)
            os_signpost(.begin, log: networkingLog, name: "Search", signpostID: spidForSearch, "%@ - Start", text)
            self.controller.getImages(withSearchTerm: text) { (items, error) in
                self.tableView.reloadData()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                os_signpost(.end, log: self.networkingLog, name: "Search", signpostID: spidForSearch, "%@ - End", text)
            }
        }
        searchBar.resignFirstResponder()
    }
}

// MARK: - UITableViewDataSource
extension ViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controller.results.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ImageCell.rawValue) as? ImageCell else {
            fatalError("Cannot dequeue cell with ID: \(Constants.ImageCell.rawValue)")
        }
        
        let item = controller.results[indexPath.row]
        
        cell.photoDescription.text = item.description ?? "n/a"
        cell.photoAuthor.text = item.author.fullName ?? "n/a"
        
        let spidForImageLoad = OSSignpostID(log: networkingLog, object: NSString(string: item.photoID))
        os_signpost(.begin, log: networkingLog, name: "Image Load", signpostID: spidForImageLoad, "%@ - Start", item.photoID)
        controller.getData(withURL: item.thumbURL) { (thumbURL, imageData, error) in
            os_signpost(.end, log: self.networkingLog, name: "Image Load", signpostID: spidForImageLoad, "%@ - End", item.photoID)
            guard let rawData = imageData, error == nil else {
                return
            }
            
            if item.thumbURL == thumbURL {
                cell.thumbnail.image = UIImage(data: rawData, scale: UIScreen.main.scale)
            }
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ViewController {
    
    // I would have prefered something a bit more complex. Like downloading multiple images at the same time. But, unfortunately, I have no time to implement it :(
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.setDownloadingState(indexPath)
        
        let item = self.controller.results[indexPath.row]
        let spidForImageSave = OSSignpostID(log: networkingLog, object: NSString(string: item.photoID))
        os_signpost(.begin, log: networkingLog, name: "Image Save", signpostID: spidForImageSave, "%@ - Start", item.photoID)
        self.controller.getData(withURL: item.photoURL) { (url, imageData, error) in
            
            self.setFinishedDownloading()
            
            if let rawData = imageData, let image = UIImage(data: rawData) {
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                os_signpost(.event, log: self.poiLog, name: "Save successful", signpostID: spidForImageSave, "%@ - Saved", item.photoID)
            }
            
            os_signpost(.begin, log: self.networkingLog, name: "Image Save", signpostID: spidForImageSave, "%@ - End", item.photoID)
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeMutableRawPointer) {
        
        if let anError = error {
            let alert = UIAlertController(title: "Error Saving Photo", message: anError.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func setDownloadingState(_ indexPath: IndexPath) {
        self.tableView.alpha = 0.7
        self.tableView.isUserInteractionEnabled = false
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? ImageCell {
            cell.downloadingState()
        }
    }
    
    private func setFinishedDownloading() {
        self.tableView.alpha = 1.0
        self.tableView.isUserInteractionEnabled = true
        
        if let selectedIndexPath = self.tableView.indexPathForSelectedRow,
            let cell = self.tableView.cellForRow(at: selectedIndexPath) as? ImageCell
        {
            cell.finishedDownloading()
            self.tableView.deselectRow(at: selectedIndexPath, animated: true)
        }
    }
}

// MARK: - UIScrollViewDelegate
extension ViewController {
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let lastVisibleCell = self.tableView.indexPathsForVisibleRows?.last,
            self.isApproachingEndOfTable(indexPath: lastVisibleCell)
            && self.controller.hasMoreResults
        {
            let spidForPrefetch = OSSignpostID(log: networkingLog)
            os_signpost(.event, log: poiLog, name: "Prefetching Results", signpostID: spidForPrefetch)
            
            let previousCount = self.controller.results.count
            self.controller.loadMoreResults { (items, error) in
                
                let newCount = self.controller.results.count
                let indexPaths = self.indexPathRange(from: previousCount, to: (newCount - 1))
                
                if indexPaths.isEmpty == false {
                    self.tableView.beginUpdates()
                    self.tableView.insertRows(at: indexPaths, with: .fade)
                    self.tableView.endUpdates()
                }
            }
        }
    }
    
    // Start preloading the results when we approach the end of the table.
    private func isApproachingEndOfTable(indexPath: IndexPath) -> Bool {
        return indexPath.row + 4 >= self.controller.results.count
    }
    
    private func indexPathRange(from: Int, to: Int) -> [IndexPath] {
        guard from <= to else {
            return []
        }
        
        var results: [IndexPath] = []
        
        for i in from...to {
            let indexPath = IndexPath.init(row: i, section: 0)
            results.append(indexPath)
        }
        
        return results
    }
}
