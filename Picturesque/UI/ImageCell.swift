//
//  ImageCell.swift
//  Picturesque
//
//  Created by Dejan on 09/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var photoDescription: UILabel!
    @IBOutlet weak var photoAuthor: UILabel!
    
    @IBOutlet weak var downloading: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.thumbnail.image = nil
        self.photoDescription.text = nil
        self.photoAuthor.text = nil
    }
    
    public func downloadingState() {
        self.downloading.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    public func finishedDownloading() {
        self.downloading.isHidden = true
        self.activityIndicator.stopAnimating()
    }
}
