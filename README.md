# ATPicturesque

A simple app that connects to [unsplash.com](https://unsplash.com/) and searches for images. This project will be used on https://agostini.tech/ as a demo project for one of the articles.